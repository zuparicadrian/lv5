﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class DataConsolePrinter
    {
        public void print(IDataset dataset)
        {
            if (dataset.GetData() == null)
            {
                Console.WriteLine("Pravo pristupa nije odobreno!");
            }
            else
            {
                foreach(List<string> a in dataset.GetData())
                {
                    foreach(string b in a)
                    {
                        Console.Write(b+" ");
                    }
                    Console.WriteLine();
                }
            }
        }
    }
}
